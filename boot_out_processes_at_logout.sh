#!/bin/bash

# Name: Stop processes as last user from running when user is logged out
# Date created: 10/09/2019

# Booting out user from launchdaemons
/bin/launchctl bootout user/$UID 

# temporarily disabling the cfprefd to clean up extra processes
/bin/launchctl disable user/$UID/com.apple.cfprefsd.xpc.agent

# Reenabling cfprefsd
/bin/launchctl enable user/$UID/com.apple.cfprefsd.xpc.agent

exit 0